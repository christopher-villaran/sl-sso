import { IconProps } from '@components/basic/icon/Icon.types';

export type NavItemProps = Pick<IconProps, 'icon'> & {
  extStyles?: string;
  text: string;
};
