// Libraries
import { FC } from 'react';
import classnames from 'classnames';

// Components
import { Icon } from '@components/basic';

// Types
import { NavItemProps } from './NavItem.types';

// Styles
import styles from './NavItem.module.sass';

const Nav: FC<NavItemProps> = (props) => {
  const { text, icon, extStyles } = props;

  return (
    <li className={classnames(extStyles, styles.navItem)}>
      <Icon icon={icon} extStyles={styles.navItemIcon} />
      <span>{text}</span>
    </li>
  );
};

export default Nav;
