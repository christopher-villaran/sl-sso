// Libraries
import { FC } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faHome,
  faPhoneAlt,
  faUser,
  faLock,
  faInfoCircle,
} from '@fortawesome/free-solid-svg-icons';
import classnames from 'classnames';

// Types
import { IconProps } from './Icon.types';

// Styles
import styles from './Icon.module.css';

library.add(faHome);
library.add(faPhoneAlt);
library.add(faUser);
library.add(faLock);
library.add(faInfoCircle);

const Icon: FC<IconProps> = ({ icon, extStyles = '' }) => (
  <div className={classnames(extStyles, styles.icon)}>
    <FontAwesomeIcon icon={['fas', icon]} />
  </div>
);

export default Icon;
