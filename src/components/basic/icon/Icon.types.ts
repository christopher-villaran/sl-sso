export type IconProps = {
  icon: 'home' | 'info-circle' | 'lock' | 'phone-alt' | 'user';
  extStyles?: string;
};
