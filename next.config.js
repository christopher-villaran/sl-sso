const isProd = process.env.NODE_ENV === 'production'

const nextConfig = {
  distDir: 'build/_next',
  reactStrictMode: true,
}

if (isProd) {
  nextConfig.assetPrefix = 'http://localhost:4566/sl-sso';
}

module.exports = nextConfig;