import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Icon from './Icon';

export default {
  component: Icon,
  title: 'Basic/Icon',
} as ComponentMeta<typeof Icon>;

const Template: ComponentStory<typeof Icon> = (args) => <Icon {...args} />;

export const Home = Template.bind({});

Home.args = {
  icon: 'home',
};

export const User = Template.bind({});

User.args = {
  icon: 'user',
};

export const PhoneAlt = Template.bind({});

PhoneAlt.args = {
  icon: 'phone-alt',
};

export const Lock = Template.bind({});

Lock.args = {
  icon: 'lock',
};

export const InfoCircle = Template.bind({});

InfoCircle.args = {
  icon: 'info-circle',
};
