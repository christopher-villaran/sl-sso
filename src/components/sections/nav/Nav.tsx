// Libraries
import { FC } from 'react';
import classnames from 'classnames';

// Components
import { NavItem } from '@components/complex';

// Types
import { NavProps } from './Nav.types';

// Styles
import styles from './Nav.module.sass';

const Nav: FC<NavProps> = (props) => {
  const { extStyles } = props;

  return (
    <nav className={classnames(extStyles, styles.nav)}>
      <ul>
        <NavItem text="Home" icon="home" extStyles={styles.active} />
        <NavItem text="Personal Info" icon="user" />
        <NavItem text="Contact & Address" icon="phone-alt" />
        <NavItem text="Security" icon="lock" />
        <NavItem text="About" icon="info-circle" />
      </ul>
    </nav>
  );
};

export default Nav;
